//
//  OfferHeaderCell.h
//  GoldCasino
//
//  Created by SKETCH_IOS_01 on 27/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfferHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnAllOffer;
@property (weak, nonatomic) IBOutlet UIButton *btnMyOffer;
@property (weak, nonatomic) IBOutlet UIButton *btnScan;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblTarget;
@property (weak, nonatomic) IBOutlet UILabel *lblTier;
@property (weak, nonatomic) IBOutlet UILabel *lblPoints;

@end
