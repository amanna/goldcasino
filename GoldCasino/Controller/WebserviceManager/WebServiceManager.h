//
//  WebServiceManager.h
//  WovaxUniversalApp
//
//  Created by Susim Samanta on 11/11/13.
//  Copyright (c) 2013 Wovax, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^FetchCompletionHandler)(id object, NSError *error);
@interface WebServiceManager : NSObject
+(void)loginOnCompletion:(NSString*)strUserName andPass:(NSString*)strPass onCompletion:(FetchCompletionHandler)handler;
+(void)getOfferListOnCompletion:(FetchCompletionHandler)handler;
+(void)getMyOfferListOnCompletion:(FetchCompletionHandler)handler;
+(void)saveMyOffer:(NSString*)strOffer onCompletion:(FetchCompletionHandler)handler;
+(void)saveMyDeviceToken:(NSString*)strOffer onCompletion:(FetchCompletionHandler)handler;
+(void)saveMyLatLongitude:(NSString*)strLatt andLoni:(NSString*)strLoni onCompletion:(FetchCompletionHandler)handler;

+(void)getSectionWisePageListOnCompletion:(FetchCompletionHandler)handler;
+(void)getSubSectionWisePageListOnCompletion:(NSString *)strSection onCompletion:(FetchCompletionHandler)handler;
//+ (void)fetchImageDataWithLink:(NSString *)imageLink OnCompletion:(FetchCompletionHandler)handler;
//+ (void)fetchRecentPostOnCompletion:(FetchCompletionHandler)handler;
//+ (void)fetchPostDetails:(NSString *)strPostId onCompletion:(FetchCompletionHandler)handler;
//+ (void)fetchMenuDetails:(NSString*)strMenuId withPage:(NSInteger)page onCompletion:(FetchCompletionHandler)handler;
//+ (void)fetchSearchDetails:(NSString *)strSearch onCompletiion:(FetchCompletionHandler)handler;
//+ (void)submitComment:(NSString *)strPostId  withText:(NSString*)strComment  onCompletion:(FetchCompletionHandler)handler;
//+ (void)registerRemoteNotificationWithDeviceId:(NSString *)deviceToken onCompletion:(FetchCompletionHandler)handler;
//+ (void)fetchPostTypeDetails:(NSString *)strPostId withType:(NSString*)strType onCompletion:(FetchCompletionHandler)handler;
//+ (void)fetchRecentPostPagewiseOnCompletion:(NSInteger)page onCompletion:(FetchCompletionHandler)handler;
//+ (NSString *)URLEncodeStringFromString:(NSString *)string;
//+ (void)callClearBadgeTokenServiceOncompletion:(FetchCompletionHandler)handler;
//+ (void)callMapService:(FetchCompletionHandler)handler;
//+ (void)fetchtagMenuDetails:(NSString*)strMenuId withPage:(NSInteger)page onCompletion:(FetchCompletionHandler)handler;
//+ (void)storeDeviceMeataDataToBackendOnCompletion:(FetchCompletionHandler)handler;
//+ (void)getAppSettingsDataOnCompletion:(FetchCompletionHandler)handler;
//+ (void)getWovaxSettingsDataOnCompletion:(FetchCompletionHandler)handler;
//+ (void)fetchRecentPostDictOfPage:(NSInteger)page onCompletion:(FetchCompletionHandler)handler ;
//+ (void)fetchNotificationOfDeviceId:(NSString *)deviceId OnCompletion:(FetchCompletionHandler)handler;
//+ (void)unsubscribePushNotificationOnCatergories:(NSString *)categories deviceId:(NSString *)deviceId onCompletion:(FetchCompletionHandler)handler ;
@end

//{"Success":true,"msg":"Login successfully","User_Details":{"Name":"Chandrahas","TargetId":"2022","UserPoints":5647,"Tire":"Platinum"}}

//offer list
//{"Success":true,"msg":"Offer Found","OfferList":[{"OfferId":11,"OfferName":"Free Play Cash!","OfferDescription":"Free Play Cash","OfferCode":"FPC","OfferValidityPeriod":"","OfferExpDate":"1\/15\/2017 12:00:00 AM","OfferImage":"https:\/\/landingpage.experiture.com\/TrackingSystem\/AG_44625\/AD_44626\/Images\/all_off_1.jpg"}]}


//http://api.experiture.com/rest/v1/json/getOfferDetailListRest/27CCC3A558E542DD827649EED27F64C3/22.4696554/88.393355/2023/0
//http://api.experiture.com/rest/v1/json/getOfferDetailListRest/{apikey}/{Latitude}/{Longitude}/{Targetid}/{OfferId}
//
//http://api.experiture.com/rest/v1/json/getMyOfferListRest/27CCC3A558E542DD827649EED27F64C3/22.4696554/88.393355/2023/0
//http://api.experiture.com/rest/v1/json/getMyOfferListRest/{apikey}/{Latitude}/{Longitude}/{Targetid}/{OfferId}
//
//http://api.experiture.com/rest/v1/json/saveMyOfferRest/27CCC3A558E542DD827649EED27F64C3/2023/11
//http://api.experiture.com/rest/v1/json/saveMyOfferRest/{apikey}/{Targetid}/{OfferId}
