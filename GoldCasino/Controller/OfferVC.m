//
//  OfferVC.m
//  GoldCasino
//
//  Created by MAPPS MAC on 14/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "OfferVC.h"
#import "OfferCell.h"
#import "OfferHeaderCell.h"
#import "WebServiceManager.h"
#import "Offer.h"
#import "MyOfferCell.h"
#import "POAcvityView.h"
#define btnGlobal 6000
@interface OfferVC ()<UITableViewDataSource,UITableViewDelegate>{
    OfferHeaderCell *cellH ;
    POAcvityView *activity;
}
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property(nonatomic)NSMutableArray *arrOfferList;
@property(nonatomic)NSMutableArray *arrMyOfferList;
@property(nonatomic)BOOL isMyOffer;
- (IBAction)btnNoticeAction:(UIButton *)sender;

@end

@implementation OfferVC

- (void)viewDidLoad {
    [super viewDidLoad];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
    
    self.isMyOffer = NO;
    self.arrOfferList = [[NSMutableArray alloc]init];
    self.arrMyOfferList = [[NSMutableArray alloc]init];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [singleFingerTap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:singleFingerTap];
    self.tblView.hidden = true;
   // [self getOfferList];
    
    
    // Do any additional setup after loading the view.
}
- (void)getOfferList{
    [activity showView];
    if(self.isMyOffer == NO){
        [WebServiceManager getOfferListOnCompletion:^(id object, NSError *error) {
            [activity hideView];
            if(object){
                self.arrOfferList = object;
                self.tblView.separatorColor = [UIColor clearColor];
                CGFloat tableHeight = self.arrOfferList.count * 73.0f+ 1*195;
                [self.tblView setContentSize:CGSizeMake(self.tblView.contentSize.width, tableHeight)];
                [self.tblView reloadData];
                self.tblView.hidden = false;
            }else{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Offer" message:@"NO Offer found" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alertView show];
            }
        }];

    }else{
        [WebServiceManager getMyOfferListOnCompletion:^(id object, NSError *error) {
            [activity hideView];
            if(object){
                self.arrMyOfferList = object;
                self.tblView.separatorColor = [UIColor clearColor];
                CGFloat tableHeight = self.arrMyOfferList.count * 73.0f+ 1*195;
                [self.tblView setContentSize:CGSizeMake(self.tblView.contentSize.width, tableHeight)];
                [self.tblView reloadData];
                self.tblView.hidden = false;
            }else{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Offer" message:@"NO Offer found" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alertView show];
            }
        }];

    }
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
   eventCompletionHandlerO(@"1",kOfferTap);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setEventOnCompletion:(EventCompletionHandlerO)handler{
    [activity showView];
    if(self.isMyOffer == NO){
        [WebServiceManager getOfferListOnCompletion:^(id object, NSError *error) {
            [activity hideView];
            eventCompletionHandlerO = handler;
            if(object){
                self.arrOfferList = object;
                self.tblView.separatorColor = [UIColor clearColor];
                CGFloat tableHeight = self.arrOfferList.count * 73.0f+ 1*195;
                [self.tblView setContentSize:CGSizeMake(self.tblView.contentSize.width, tableHeight)];
                [self.tblView reloadData];
                self.tblView.hidden = false;
            }else{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Offer" message:@"NO Offer found" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alertView show];
            }
        }];
        
    }else{
        [WebServiceManager getMyOfferListOnCompletion:^(id object, NSError *error) {
            [activity hideView];
            eventCompletionHandlerO = handler;
            if(object){
                self.arrMyOfferList = object;
                self.tblView.separatorColor = [UIColor clearColor];
                CGFloat tableHeight = self.arrMyOfferList.count * 73.0f+ 1*195;
                [self.tblView setContentSize:CGSizeMake(self.tblView.contentSize.width, tableHeight)];
                [self.tblView reloadData];
                self.tblView.hidden = false;
            }else{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Offer" message:@"NO Offer found" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alertView show];
            }
        }];
        
    }

    
}

- (IBAction)btnMenuAction:(UIButton *)sender {
    if(self.view.frame.origin.x==0){
        eventCompletionHandlerO(@"0",kOfferTapMenu);
    }else{
        eventCompletionHandlerO(@"1",kOfferTapMenu);
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   if(self.isMyOffer == YES){
       return  self.arrMyOfferList.count + 1;
   }else{
        return  self.arrOfferList.count + 1;
   }
}
// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
 if(indexPath.row==0){
       cellH = [tableView dequeueReusableCellWithIdentifier:@"CellH"];
       if(!cellH){
           [tableView registerNib:[UINib nibWithNibName:@"OfferHeaderCell" bundle:nil] forCellReuseIdentifier:@"CellH"];
           cellH = [tableView dequeueReusableCellWithIdentifier:@"CellH"];
       }
     NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
     NSString *strUname = [prefs stringForKey:@"username"];
     NSString *strUserPoint = [prefs stringForKey:@"userpoints"];
     NSString *strTire = [prefs stringForKey:@"Tire"];
     NSString *strPlayer = [prefs stringForKey:@"PlayerId"];
     cellH.lblUserName.text = [NSString stringWithFormat:@"Hello, %@!",strUname];
     cellH.lblTarget.text = [NSString stringWithFormat:@"Player ID: %@",strPlayer];
     cellH.lblTier.text = [NSString stringWithFormat:@"Tier:%@",strTire];
     cellH.lblPoints.text = [NSString stringWithFormat:@"Points:%@",strUserPoint];
     
     [cellH.btnAllOffer addTarget:self action:@selector(btnAllOfferAction:) forControlEvents:UIControlEventTouchUpInside];
     [cellH.btnMyOffer addTarget:self action:@selector(btnMyOfferAction:) forControlEvents:UIControlEventTouchUpInside];
     [cellH.btnScan addTarget:self action:@selector(btnScanAction:) forControlEvents:UIControlEventTouchUpInside];
       cellH.selectionStyle = UITableViewCellSelectionStyleNone;
       return cellH;
   }
   else{
      if(self.isMyOffer == YES){
          Offer *offer = [self.arrMyOfferList objectAtIndex:indexPath.row - 1];
          MyOfferCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellM"];
          if(!cell){
              [tableView registerNib:[UINib nibWithNibName:@"MyOfferCell" bundle:nil] forCellReuseIdentifier:@"CellM"];
              cell = [tableView dequeueReusableCellWithIdentifier:@"CellM"];
          }
          cell.tag = indexPath.row;
          cell.TitleOffer.text = offer.strOfferName;
          
          NSArray *arr = [offer.strOfferExp componentsSeparatedByString:@" "];
          cell.subTitleOffer.text = [NSString stringWithFormat:@"Exp.%@",[arr objectAtIndex:0]];
          dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
          dispatch_async(queue, ^(void) {
              
              NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:offer.strOfferImage]];
              
              UIImage* image = [[UIImage alloc] initWithData:imageData];
              if (image) {
                  dispatch_async(dispatch_get_main_queue(), ^{
                      if (cell.tag == indexPath.row) {
                          cell.imgOffer.image = image;
                          [cell setNeedsLayout];
                      }
                  });
              }
          });

          cell.selectionStyle = UITableViewCellSelectionStyleNone;
          return cell;
      }else{
          Offer *offer = [self.arrOfferList objectAtIndex:indexPath.row - 1];
          OfferCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
          if(!cell){
              [tableView registerNib:[UINib nibWithNibName:@"OfferCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
              cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
          }
          cell.btnSave.tag = btnGlobal + [offer.strOfferId integerValue];
          [cell.btnSave addTarget:self action:@selector(btnSaveAction:) forControlEvents:UIControlEventTouchUpInside];
          cell.tag = indexPath.row;
          cell.TitleOffer.text = offer.strOfferName;
          
          NSArray *arr = [offer.strOfferExp componentsSeparatedByString:@" "];
          cell.subTitleOffer.text = [NSString stringWithFormat:@"Exp.%@",[arr objectAtIndex:0]];
          dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
          dispatch_async(queue, ^(void) {
              
              NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:offer.strOfferImage]];
                                   
                                   UIImage* image = [[UIImage alloc] initWithData:imageData];
                                   if (image) {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           if (cell.tag == indexPath.row) {
                                               cell.imgOffer.image = image;
                                               [cell setNeedsLayout];
                                           }
                                       });
                                   }
            });
          cell.selectionStyle = UITableViewCellSelectionStyleNone;
          return cell;
      }
    }
}
- (void)btnSaveAction:(UIButton*)btn{
    [activity showView];
    NSInteger offerId = btn.tag - btnGlobal;
    NSString *strOffer = [NSString stringWithFormat:@"%ld",offerId];
    [WebServiceManager saveMyOffer:strOffer onCompletion:^(id object, NSError *error) {
        [activity hideView];
        if(object){
            NSString *str = object;
            if([str isEqualToString:@"1"]){
                
            }else{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Offer" message:@"This Offer alredy exist." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alertView show];
            }
        }else{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Login" message:@"Some Problem Occurs!!Please try again later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alertView show];
        }
    }];
    
}
- (void)btnScanAction:(UIButton*)btn{
    eventCompletionHandlerO(nil,kOfferScan);
}
- (void)btnAllOfferAction:(UIButton*)btn{
    
    [cellH.btnAllOffer setBackgroundImage:[UIImage imageNamed:@"offer-red-bg.png"] forState:UIControlStateNormal];
    [cellH.btnMyOffer setBackgroundImage:[UIImage imageNamed:@"offer-black-bg.png"] forState:UIControlStateNormal];
    self.isMyOffer= NO;
    [self getOfferList];
}
- (void)btnMyOfferAction:(UIButton*)btn{
    [cellH.btnMyOffer setBackgroundImage:[UIImage imageNamed:@"offer-red-bg.png"] forState:UIControlStateNormal];
    [cellH.btnAllOffer setBackgroundImage:[UIImage imageNamed:@"offer-black-bg.png"] forState:UIControlStateNormal];
    self.isMyOffer= YES;
    [self getOfferList];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if(indexPath.row==0 || indexPath.row==3 ){
//    }else{
//        eventCompletionHandlerHot(nil,kHotelDetails);
//    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   if(indexPath.row==0){
       return 195;
   }else{
       return 73;
   }
    //return 73;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnNoticeAction:(UIButton *)sender {
     eventCompletionHandlerO(nil,kOfferNotify);
}
@end
