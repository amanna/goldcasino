//
//  FacebookStyleViewController.m
//  BLKFlexibleHeightBar Demo
//
//  Created by Bryan Keller on 3/7/15.
//  Copyright (c) 2015 Bryan Keller. All rights reserved.
//

#import "FacebookStyleViewController.h"
#import "GoldCell.h"
#import "FacebookStyleBar.h"
#import "FacebookStyleBarBehaviorDefiner.h"
#import "BLKDelegateSplitter.h"
#import "GoldCellHeader.h"
#import "SquareCashStyleBehaviorDefiner.h"
@interface FacebookStyleViewController () <UITableViewDataSource,UITableViewDelegate>
@property (nonatomic) BLKDelegateSplitter *delegateSplitter;
@property (nonatomic) FacebookStyleBar *myCustomBar;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation FacebookStyleViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setNeedsStatusBarAppearanceUpdate];
  
    self.myCustomBar = [[FacebookStyleBar alloc]initWithFrame:CGRectMake(0, 0,self.view.frame.size.width ,241 )];
     self.tableView.contentInset = UIEdgeInsetsMake(self.myCustomBar.maximumBarHeight, 0.0, 0.0, 0.0);
    SquareCashStyleBehaviorDefiner *behaviorDefiner = [[SquareCashStyleBehaviorDefiner alloc] init];
    [behaviorDefiner addSnappingPositionProgress:0.0 forProgressRangeStart:0.0 end:0.5];
    [behaviorDefiner addSnappingPositionProgress:1.0 forProgressRangeStart:0.5 end:1.0];
    behaviorDefiner.snappingEnabled = YES;
    behaviorDefiner.elasticMaximumHeightAtTop = YES;
    self.myCustomBar.behaviorDefiner = behaviorDefiner;
    [self.view addSubview:self.myCustomBar];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)closeViewController:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


# pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    return 2;
//}
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//    if(section==0){
//        return @"Games";
//    }else{
//        return @"Winners";
//    }
//}
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    NSArray *list = [[NSArray alloc]initWithObjects:@"Games",@"Winners" ,nil];
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 24)];
//    /* Create custom view to display section header... */
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 22)];
//    label.textColor = [UIColor whiteColor];
//    [label setFont:[UIFont boldSystemFontOfSize:16]];
//    NSString *string =[list objectAtIndex:section];
//    /* Section header is in 0th index... */
//    [label setText:string];
//    [view addSubview:label];
//    [view setBackgroundColor:[UIColor blackColor]]; //your background color...
//    return view;
//}
// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        GoldCellHeader *cell = [tableView dequeueReusableCellWithIdentifier:@"CellH"];
        if(!cell){
            [tableView registerNib:[UINib nibWithNibName:@"GoldCellHeader" bundle:nil] forCellReuseIdentifier:@"CellH"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"CellH"];
        }
        cell.lblContent.text = @"Games";
        return cell;
    }else{
        GoldCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if(!cell){
            [tableView registerNib:[UINib nibWithNibName:@"GoldCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        }
        return cell;
        

    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        return 62;
    }else{
      return 102;
    }
}
@end
