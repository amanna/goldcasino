//
//  WinnerVC.h
//  GoldCasino
//
//  Created by MAPPS MAC on 11/02/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "ViewController.h"
enum EventTypeW {
    kWBack = 0
    };
typedef void (^EventCompletionHandlerW)(id object, NSUInteger EventTypeW);

@interface WinnerVC : ViewController{
    EventCompletionHandlerW eventCompletionHandlerW;
}
@property (weak, nonatomic) IBOutlet UILabel *lblNav;
- (void)setEventOnCompletionW:(EventCompletionHandlerW)handler;


@end
