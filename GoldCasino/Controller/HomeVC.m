//
//  HomeVC.m
//  GoldCasino
//
//  Created by MAPPS MAC on 14/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "HomeVC.h"
#import "MenuVC.h"
#import "AppDelegate.h"
#import "AboutVC.h"
@interface HomeVC (){
    AppDelegate *delegate;
    
    
}
- (IBAction)btnNotificationAction:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;
- (IBAction)btnMenuAction:(id)sender;
@property(nonatomic)MenuVC *menuVc;
- (IBAction)btnLoginAction:(UIButton *)sender;
- (IBAction)btnScanAction:(UIButton *)sender;

@end

@implementation HomeVC
- (void)setEventOnCompletion:(EventCompletionHandlerH)handler {
    eventCompletionHandlerH = handler;
}

-(void)swipeHandlerLeft:(id)sender {
    
    if(self.view.frame.origin.x==0){
        eventCompletionHandlerH(@"0",kActionTapMenu);
    }else{
        eventCompletionHandlerH(@"1",kActionTapMenu);
    }
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
//    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerLeft:)];
//    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
//    [self.view addGestureRecognizer:gestureRecognizer];

    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
        delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"AboutNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"SocialNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"SettingsNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"NightNotification"
                                               object:nil];
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
   eventCompletionHandlerH(@"1",kActionTap);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) receiveTestNotification:(NSNotification *) notification
{
    if([notification.name isEqual:@"AboutNotification"]){
        [self performSegueWithIdentifier:@"segueAbout" sender:self];
    }else if ([notification.name isEqual:@"SocialNotification"]){
         [self performSegueWithIdentifier:@"segueSocial" sender:self];
    }else if ([notification.name isEqual:@"SettingsNotification"]){
         [self performSegueWithIdentifier:@"segueSettings" sender:self];
    }else if ([notification.name isEqual:@"NightNotification"]){
         [self performSegueWithIdentifier:@"segueNight" sender:self];
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnMenuAction:(id)sender {
   if(self.view.frame.origin.x==0){
         eventCompletionHandlerH(@"0",kActionTapMenu);
    }else{
         eventCompletionHandlerH(@"1",kActionTapMenu);
    }
}
- (IBAction)btnNotificationAction:(id)sender {
     eventCompletionHandlerH(nil,kActionNotify);
    //[self performSegueWithIdentifier:@"segueAbout" sender:self];
}
- (IBAction)btnHotelAction:(UIButton *)sender {
     eventCompletionHandlerH(nil,kActionHotel);
}

- (IBAction)btnMediaAction:(UIButton *)sender {
     eventCompletionHandlerH(nil,kActionSocial);
}

- (IBAction)btnEventAction:(UIButton *)sender {
     if(self.view.frame.origin.x==0){
     eventCompletionHandlerH(nil,kActionEvent);
     }else{
          eventCompletionHandlerH(@"1",kActionTap);
     }
}

- (IBAction)btnNightAction:(UIButton *)sender {
    if(self.view.frame.origin.x==0){
     eventCompletionHandlerH(nil,kActionNight);
    }else{
         eventCompletionHandlerH(@"1",kActionTap);
    }
}
- (IBAction)btnCasinoAction:(UIButton *)sender {
     if(self.view.frame.origin.x==0){
         eventCompletionHandlerH(nil,kActionCasino);
     }else{
         eventCompletionHandlerH(@"1",kActionTap);
     }
    
}

- (IBAction)btnDiningAction:(UIButton *)sender {
     eventCompletionHandlerH(nil,kActionDining);
}
- (IBAction)btnLoginAction:(UIButton *)sender {
      eventCompletionHandlerH(nil,kActionAccount);
}

- (IBAction)btnScanAction:(UIButton *)sender {
     eventCompletionHandlerH(nil,kScan);
}
- (IBAction)btnOfferAction:(UIButton *)sender {
    eventCompletionHandlerH(nil,kActionOffer);
}
@end
