//
//  DiningVC.m
//  GoldCasino
//
//  Created by MAPPS MAC on 14/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "DiningVC.h"
#import "GoldCell.h"
#import "GoldCellHeader.h"
#import "HeaderCell.h"
#import "AppDelegate.h"
#import "Dining.h"

@interface DiningVC ()<UITableViewDataSource,UITableViewDelegate>{
    AppDelegate *delegate;
}
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property(nonatomic)NSMutableArray *arrDining;


@end

@implementation DiningVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.arrDining = [[NSMutableArray alloc]init];
    
    
    for (int i=0; i<3; i++) {
        Dining *gallery = [[Dining alloc]constructDinner:i];
        [self.arrDining addObject:gallery];
    }
    

    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [singleFingerTap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:singleFingerTap];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.tblView.separatorColor = [UIColor clearColor];
    CGFloat tableHeight = (self.arrDining.count) * 102.0f+ 1*52;
    [self.tblView setContentSize:CGSizeMake(self.tblView.contentSize.width, tableHeight)];
    // Do any additional setup after loading the view.
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
   if(self.view.frame.origin.x==0){
        self.tblView.allowsSelection = YES;
         eventCompletionHandlerD(@"1",kDinTap);
    }else{
        self.tblView.allowsSelection = NO;
         eventCompletionHandlerD(@"1",kDinTap);
    }
}

- (void)setEventOnCompletion:(EventCompletionHandlerD)handler{
    eventCompletionHandlerD = handler;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // 1. Dequeue the custom header cell
    HeaderCell* cell = [tableView dequeueReusableCellWithIdentifier:@"hCell"];
    if(!cell){
        [tableView registerNib:[UINib nibWithNibName:@"MenuHead" bundle:nil] forCellReuseIdentifier:@"hCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"hCell"];
    }
    cell.imgBanner.image = [UIImage imageNamed:@"bannerd.png"];
    [cell.btnMenu addTarget:self action:@selector(btnMenuAction) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnCasino addTarget:self action:@selector(btnCasinoAction) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnDining addTarget:self action:@selector(btnDiningAction) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnEvent addTarget:self action:@selector(btnEventAction) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnHotel addTarget:self action:@selector(btnHotelAction) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnNight addTarget:self action:@selector(btnNightAction) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnNotice addTarget:self action:@selector(btnNoticeAction) forControlEvents:UIControlEventTouchUpInside];

    return cell;
}
- (void)btnNoticeAction{
    eventCompletionHandlerD(nil,kNotifyD);
}
- (void)btnMenuAction{
    if(self.view.frame.origin.x==0){
        eventCompletionHandlerD(@"0",kDinTapMenu);
    }else{
        eventCompletionHandlerD(@"1",kDinTapMenu);
    }
}
- (void)btnCasinoAction{
    eventCompletionHandlerD(nil,kDinCasino);
}
- (void)btnDiningAction{
    eventCompletionHandlerD(nil,kDinDining);
    
}
- (void)btnEventAction{
    eventCompletionHandlerD(nil,kDinEvent);
}
- (void)btnNightAction{
    eventCompletionHandlerD(nil,kDinNight);
}
- (void)btnHotelAction{
    eventCompletionHandlerD(nil,kDinHotel);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 241 + 59 - 42;
}
// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        GoldCellHeader *cell = [tableView dequeueReusableCellWithIdentifier:@"CellH"];
        if(!cell){
            [tableView registerNib:[UINib nibWithNibName:@"GoldCellHeader" bundle:nil] forCellReuseIdentifier:@"CellH"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"CellH"];
        }
        cell.lblContent.text = @"Dining";
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        NSInteger row;
        if(indexPath.row <4){
            row = (indexPath.row - 1);
        }else{
            row = (indexPath.row - 2);
        }
        Dining *gallery = [self.arrDining objectAtIndex:row];
        GoldCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if(!cell){
            [tableView registerNib:[UINib nibWithNibName:@"GoldCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
            
        }
        cell.lblTitle.text = gallery.strTitle;
        cell.lblDesc.text = gallery.strSubTitle;
        cell.imgGallery.image = [UIImage imageNamed:gallery.strImage];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row==0){
    }else{
        NSInteger row;
        if(indexPath.row <4){
            row = (indexPath.row - 1);
        }else{
            row = (indexPath.row - 2);
        }
        Dining *gallery = [self.arrDining objectAtIndex:row];
        eventCompletionHandlerD(gallery,kDinDetails);
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        return 52;
    }else{
        return 102;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
