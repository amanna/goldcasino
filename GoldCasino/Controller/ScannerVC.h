//
//  ScannerVC.h
//  POP
//
//  Created by Tuli on 6/10/14.
//  Copyright (c) 2014 Tuli e services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Barcode.h"
enum EventScan {
    kScanBack = 0
   };
typedef void (^EventCompletionHandlerSc)(id object, NSUInteger EventScan);


@interface ScannerVC : UIViewController{
    EventCompletionHandlerSc eventCompletionHandlerSc;
}
- (void)setEventOnCompletionSC:(EventCompletionHandlerSc)handler;

@property(nonatomic)NSMutableArray *allowedBarcodeTypes;

- (IBAction)btnbackAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblNav;
@end
