//
//  FacebookStyleBar.m
//  BLKFlexibleHeightBar Demo
//
//  Created by Bryan Keller on 3/7/15.
//  Copyright (c) 2015 Bryan Keller. All rights reserved.
//

#import "FacebookStyleBar.h"

@implementation FacebookStyleBar

- (instancetype)initWithFrame:(CGRect)frame
{
    if(self = [super initWithFrame:frame])
    {
        [self configureBar];
    }
    
    return self;
}
- (void)configureBar
{
    self.maximumBarHeight = 241.0;
    self.minimumBarHeight = 20.0;
    self.backgroundColor = [UIColor redColor];
}

- (void)config1{
    self.maximumBarHeight = 241.0;
    self.minimumBarHeight = 0.0;
    
    // Configure bar appearence
    //    self.maximumBarHeight = 105.0;
    //    self.minimumBarHeight = 20.0;
    self.backgroundColor = [UIColor greenColor];
    self.clipsToBounds = YES;
    
    
    
    // Add and configure header image
    UIImageView *haederImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header.png"]];
    haederImageView.contentMode = UIViewContentModeScaleAspectFill;
    haederImageView.clipsToBounds = YES;
    
    BLKFlexibleHeightBarSubviewLayoutAttributes *initialWhiteBarLayoutAttributes = [[BLKFlexibleHeightBarSubviewLayoutAttributes alloc] init];
    initialWhiteBarLayoutAttributes.frame = CGRectMake(0.0, 0.0, self.frame.size.width, 58.0);
    [haederImageView addLayoutAttributes:initialWhiteBarLayoutAttributes forProgress:0.0];
    
    BLKFlexibleHeightBarSubviewLayoutAttributes *finalWhiteBarLayoutAttributes = [[BLKFlexibleHeightBarSubviewLayoutAttributes alloc] initWithExistingLayoutAttributes:initialWhiteBarLayoutAttributes];
    finalWhiteBarLayoutAttributes.transform = CGAffineTransformMakeTranslation(0.0, -58.0);
    [haederImageView addLayoutAttributes:finalWhiteBarLayoutAttributes forProgress:58.0/(241.0-0.0)];
    
    [self addSubview:haederImageView];
    
    
    // Add and configure Menu image
    
    UIImageView *menuImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menu-bg.png"]];
    menuImageView.contentMode = UIViewContentModeScaleAspectFill;
    menuImageView.clipsToBounds = YES;
    
    BLKFlexibleHeightBarSubviewLayoutAttributes *initialWhiteBarLayoutAttributesM = [[BLKFlexibleHeightBarSubviewLayoutAttributes alloc] init];
    initialWhiteBarLayoutAttributesM.frame = CGRectMake(0.0, 58.0, self.frame.size.width, 41.0);
    [menuImageView addLayoutAttributes:initialWhiteBarLayoutAttributesM forProgress:0.0];
    
    BLKFlexibleHeightBarSubviewLayoutAttributes *finalWhiteBarLayoutAttributesM = [[BLKFlexibleHeightBarSubviewLayoutAttributes alloc] initWithExistingLayoutAttributes:initialWhiteBarLayoutAttributesM];
    finalWhiteBarLayoutAttributesM.transform = CGAffineTransformMakeTranslation(0.0, -41.0);
    [menuImageView addLayoutAttributes:finalWhiteBarLayoutAttributesM forProgress:41.0/(241.0-0.0)];
    
    [self addSubview:menuImageView];
    
    
    // Add and configure Banner image
    
    UIImageView *bannerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"banner.png"]];
    bannerImageView.contentMode = UIViewContentModeScaleAspectFill;
    bannerImageView.clipsToBounds = YES;
    
    BLKFlexibleHeightBarSubviewLayoutAttributes *initialWhiteBarLayoutAttributesB = [[BLKFlexibleHeightBarSubviewLayoutAttributes alloc] init];
    initialWhiteBarLayoutAttributesB.frame = CGRectMake(0.0, 99.0, self.frame.size.width, 142);
    [bannerImageView addLayoutAttributes:initialWhiteBarLayoutAttributesB forProgress:0.0];
    
    BLKFlexibleHeightBarSubviewLayoutAttributes *finalWhiteBarLayoutAttributesB = [[BLKFlexibleHeightBarSubviewLayoutAttributes alloc] initWithExistingLayoutAttributes:initialWhiteBarLayoutAttributesB];
    finalWhiteBarLayoutAttributesB.transform = CGAffineTransformMakeTranslation(0.0, -99.0);
    [bannerImageView addLayoutAttributes:finalWhiteBarLayoutAttributesB forProgress:99.0/(241.0-0.0)];
    
    // [self addSubview:bannerImageView];
    

}

@end
