//
//  Hotel.m
//  GoldCasino
//
//  Created by SKETCH_IOS_01 on 29/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "Hotel.h"

@implementation Hotel
- (id)constructHotel:(NSInteger)index{
    switch (index) {
        case 0:{
            self.strTitle = @"Book your room now!";
            self.strSubTitle = @"Our best rates, Guaranteed!";
            self.strDesc = @"Test your luck at one of over 200 table games on our property. We have Blackjack, Spanish 21, Caribbean Stud Poker, Baccarat, Craps, and Roulette in addition to our exciting 42-table Poker Room. Grab a seat to get in on the action!";
            self.strImage = @"footer-hotel.png";
            self.strLargeImage = @"BananaCabana-big.png";
        }
            break;
        case 1:{
            self.strTitle = @"Amenities";
            self.strSubTitle = @"Enjoy world-class amenities";
            self.strDesc = @"<html><body><p><strong>In-room dining</strong></p><p>Satisfy any craving with a delectable food and beverage menu curated by our chef, Hans Waldrin.</p><p><strong>Concierge services</strong></p><p>Experience downtown like a local with personalized itineraries created by our concierge. As an exclusive perk, our guests receive discounts at a number of restaurants and boutiques throughout the casino property.</p><p><strong>Fitness room</strong></p><p>The Fitness Room is located on the third floor and offers wellness equipment by Techno Gym.</p><p><strong>Business services</strong></p><p>All business needs including scanning, printing, faxing and copying are made available by our Guest Reception team. Additionally, our Suites are equipped with 27-inch iMacs and iPods are also available for all guests at no additional fee. Mounted iPads are available in the hotel lobby for info on the go.</p></body></html>";
            self.strImage = @"Amenitites.png";
            self.strLargeImage = @"Amenitites-big.png";
        }
            break;
        case 2:{
            self.strTitle = @"Rooms";
            self.strSubTitle = @"From classic to luxe suite... ";
            self.strDesc = @"<html><body><p><strong>Superior queen</strong></p><p>The Superior Queen Room features Queen pillow-top mattress, down comforter & pillows, a flat-screen television, and luxury amenities.</p><p><strong>Superior king</strong></p><p>Superior King Rooms feature a Custom King bed with leather headboard and hand-finished bronze detailing.</p><p><strong>Deluxe king</strong></p><p>The spacious, 240 square foot Deluxe King Room features impressive views and a pillow top Custom King mattress.</p><p><strong>Grand Suite</strong></p><p>This exquisite 16th floor Grand Suite, spanning 700-800 square feet, features a master bedroom with a California King bed</p></body></html>";
            self.strImage = @"rooms.png";
            self.strLargeImage = @"rooms-big.png";
        }
            break;
            
        default:
            break;
    }
    return self;

}
@end
