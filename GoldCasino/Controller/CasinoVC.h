//
//  CasinoVC.h
//  GoldCasino
//
//  Created by MAPPS MAC on 14/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypeC {
    kCasTapMenu = 0,
    kCasino = 1,
    kEvent = 2,
    kDining = 3,
    kNight = 4,
    kHotel = 5,
    kDetails = 6,
    kTap=7,
    kWinner=8,
    kNotifyC=9
    
};
typedef void (^EventCompletionHandlerC)(id object, NSUInteger EventTypeC);

@interface CasinoVC : UIViewController{
     EventCompletionHandlerC eventCompletionHandlerC;
}
- (void)setEventOnCompletion:(EventCompletionHandlerC)handler ;
@property (weak, nonatomic) IBOutlet UIView *viewTrans;
@property(nonatomic)NSString *strSectionId;
@end
