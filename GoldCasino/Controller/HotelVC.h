//
//  HotelVC.h
//  GoldCasino
//
//  Created by MAPPS MAC on 14/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypeHot {
    kHotelTapMenu = 0,
    kHotelCasino = 1,
    kHotelDining = 2,
    kHotelEvent = 3,
    kHotelHotel = 4,
    kHotelNight = 5,
    kHotelTap=6,
    kHotelDetails=7,
    kReserve=8,
    kHotelNotify=9
};
typedef void (^EventCompletionHandlerHot)(id object, NSUInteger EventTypeHot);
@interface HotelVC : UIViewController{
     EventCompletionHandlerHot eventCompletionHandlerHot;
}
@property (weak, nonatomic) IBOutlet UIView *viewTrans;
- (void)setEventOnCompletion:(EventCompletionHandlerHot)handler ;
@property(nonatomic)NSString *strSectionId;
@end
