//
//  SSRestManager.m
//  SSRestClient
//
//  Created by Susim Samanta on 15/08/13.
//  Copyright (c) 2013 Susim Samanta. All rights reserved.
//

#import "SSRestManager.h"
#import "SSJsonResponseHandler.h"
#import <UIKit/UIKit.h>   
@interface SSRestManager ()
@end

@implementation SSRestManager

- (void)getHttpResponseWithBaseUrl:(NSString *)url onCompletion:(SSServiceResponseHandler)serviceHandler onError:(SSErrorHandler)errorHandler {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
    [request setValue:self.authorizationValue? self.authorizationValue :@"" forHTTPHeaderField:@"Authorization"];
    [request addValue:self.contentType? self.contentType :@"" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:self.httpMethod];
    [request setHTTPBody:[self.httpBody? self.httpBody : @"" dataUsingEncoding:NSUTF8StringEncoding]];
    [NSURLConnection sendAsynchronousRequest:(NSURLRequest *)request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (error) {
            errorHandler (error);
        }else {
            serviceHandler (data,response);
        }
    }];
}

- (void)getServiceResponseWithBaseUrl:(NSString *)baseUrl query:(NSString *)queryString onCompletion:(SSServiceResponseHandler )serviceHandler onError:(SSErrorHandler)errorHandler {
    NSString *mainUrl = [NSString stringWithFormat:@"%@%@",baseUrl,queryString];
    mainUrl = [mainUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:mainUrl ]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"text/xml"
   forHTTPHeaderField:@"Content-type"];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (error) {
            errorHandler (error);
        }else {
            serviceHandler (data,response);
        }
    }];
}

-(void)getJsonResponseFromBaseUrl:(NSString *)baseUrl query:(NSString *)queryString onCompletion:(SSJSONResponseHandler)jsonHandler onError:(SSErrorHandler)errorHandler {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self getServiceResponseWithBaseUrl:baseUrl query:queryString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            jsonHandler(json);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        } onError:^(NSError *error) {
            errorHandler(error);
             [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }];
    } onError:^(NSError *error) {
        errorHandler (error);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }];
}

@end
