//
//  SocialVC.m
//  GoldCasino
//
//  Created by Amrita on 23/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "SocialVC.h"
#import "POAcvityView.h"
@interface SocialVC (){
    POAcvityView *activity;
}

- (IBAction)btnMenuAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIWebView *myWeb;

- (IBAction)btnNoticeAction:(UIButton *)sender;
@end

@implementation SocialVC
- (void)setEventOnCompletionS:(EventCompletionHandlerS)handler {
    eventCompletionHandlerS = handler;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
    [self.myWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://app.xgoldcasino.com/social_new_app.html"]]];
    [activity showView];

    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];

    // Do any additional setup after loading the view.
}
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [activity hideView];
    NSLog(@"finish");
}


-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    [activity showView];
    NSLog(@"Error for WEBVIEW: %@", [error description]);
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    eventCompletionHandlerS(@"1",kSocialTap);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnMenuAction:(UIButton *)sender {
    if(self.view.frame.origin.x==0){
        eventCompletionHandlerS(@"0",kSocialMenu);
    }else{
        eventCompletionHandlerS(@"1",kSocialTap);
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnNoticeAction:(UIButton *)sender {
     eventCompletionHandlerS(nil,kSocialNotify);
}
@end
