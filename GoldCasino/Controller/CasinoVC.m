//
//  CasinoVC.m
//  GoldCasino
//
//  Created by MAPPS MAC on 14/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "CasinoVC.h"
#import "GoldCell.h"
#import "GoldCellHeader.h"
#import "HeaderCell.h"
#import "AppDelegate.h"
#import "DetailsVC.h"
#import "Gallery.h"
#import "Winner.h"
#import "WebServiceManager.h"
@interface CasinoVC ()<UITableViewDataSource,UITableViewDelegate>{
    AppDelegate *delegate;
    HeaderCell* cellHeader ;
}
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property(nonatomic)NSMutableArray *arrGallery;
@property(nonatomic)  UIView* coverView;

@end

@implementation CasinoVC

- (void)viewDidLoad {
    [super viewDidLoad];
       self.arrGallery = [[NSMutableArray alloc]init];
    
//    for (int i=0; i<5; i++) {
//        Gallery *gallery = [[Gallery alloc]constructGallery:i];
//        [self.arrGallery addObject:gallery];
//    }
    
   
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [singleFingerTap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:singleFingerTap];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.tblView.separatorColor = [UIColor clearColor];
    CGFloat tableHeight = (self.arrGallery.count * 102.0f)  + 2*52;
    [self.tblView setContentSize:CGSizeMake(self.tblView.contentSize.width, tableHeight)];

    // Do any additional setup after loading the view.
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    if(self.view.frame.origin.x==0){
        self.tblView.allowsSelection = YES;
    eventCompletionHandlerC(@"1",kTap);
    }else{
        self.tblView.allowsSelection = NO;
         eventCompletionHandlerC(@"1",kTap);
    }
}
- (void)setEventOnCompletion:(EventCompletionHandlerC)handler{
    if(self.strSectionId.length !=0){
        [WebServiceManager getSubSectionWisePageListOnCompletion:self.strSectionId onCompletion:^(id object, NSError *error) {
            if(object){
                 eventCompletionHandlerC = handler;
            }
        }];
        
    }else{
        eventCompletionHandlerC = handler;
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrGallery.count;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // 1. Dequeue the custom header cell
     cellHeader = [tableView dequeueReusableCellWithIdentifier:@"hCell"];
    if(!cellHeader){
        [tableView registerNib:[UINib nibWithNibName:@"MenuHead" bundle:nil] forCellReuseIdentifier:@"hCell"];
        cellHeader = [tableView dequeueReusableCellWithIdentifier:@"hCell"];
    }
    cellHeader.imgBanner.image = [UIImage imageNamed:@"bannerc.png"];
     [cellHeader.btnMenu addTarget:self action:@selector(btnMenuAction) forControlEvents:UIControlEventTouchUpInside];
     [cellHeader.btnCasino addTarget:self action:@selector(btnCasinoAction) forControlEvents:UIControlEventTouchUpInside];
     [cellHeader.btnDining addTarget:self action:@selector(btnDiningAction) forControlEvents:UIControlEventTouchUpInside];
     [cellHeader.btnEvent addTarget:self action:@selector(btnEventAction) forControlEvents:UIControlEventTouchUpInside];
     [cellHeader.btnHotel addTarget:self action:@selector(btnHotelAction) forControlEvents:UIControlEventTouchUpInside];
     [cellHeader.btnNight addTarget:self action:@selector(btnNightAction) forControlEvents:UIControlEventTouchUpInside];
     [cellHeader.btnNotice addTarget:self action:@selector(btnNoticeAction) forControlEvents:UIControlEventTouchUpInside];
    return cellHeader;
}
- (void)btnMenuAction{
    if(self.view.frame.origin.x==0){
        eventCompletionHandlerC(@"0",kCasTapMenu);
    }else{
        eventCompletionHandlerC(@"1",kCasTapMenu);
    }

}
- (void)btnNoticeAction{
     eventCompletionHandlerC(nil,kNotifyC);
}
- (void)btnCasinoAction{
   // [cell.btnCasino setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    //[cell.btnCasino setBackgroundImage:[UIImage imageNamed:@"menu-bg-hover.png"] forState:UIControlStateNormal];
     eventCompletionHandlerC(nil,kCasino);
}
- (void)btnDiningAction{
     eventCompletionHandlerC(nil,kDining);
    
}
- (void)btnEventAction{
    eventCompletionHandlerC(nil,kEvent);
}
- (void)btnNightAction{
 eventCompletionHandlerC(nil,kNight);
}
- (void)btnHotelAction{
   eventCompletionHandlerC(nil,kHotel);
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 241 + 59 - 42;
}
// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        GoldCellHeader *cell = [tableView dequeueReusableCellWithIdentifier:@"CellH"];
        if(!cell){
            [tableView registerNib:[UINib nibWithNibName:@"GoldCellHeader" bundle:nil] forCellReuseIdentifier:@"CellH"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"CellH"];
        }
        cell.lblContent.text = @"Games";
         cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else if(indexPath.row==4){
        GoldCellHeader *cell = [tableView dequeueReusableCellWithIdentifier:@"CellH"];
        if(!cell){
            [tableView registerNib:[UINib nibWithNibName:@"GoldCellHeader" bundle:nil] forCellReuseIdentifier:@"CellH"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"CellH"];
        }
        cell.lblContent.text = @"Winners";
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else{
        NSInteger row;
        if(indexPath.row <4){
            row = (indexPath.row - 1);
        }else{
            row = (indexPath.row - 2);
        }
        Gallery *gallery = [self.arrGallery objectAtIndex:row];
        GoldCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if(!cell){
            [tableView registerNib:[UINib nibWithNibName:@"GoldCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        }
        cell.lblTitle.text = gallery.strTitle;
        cell.lblDesc.text = gallery.strSubTitle;
        cell.imgGallery.image = [UIImage imageNamed:gallery.strImage];
         cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        }

    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row==0 || indexPath.row==4){
        
    }else{
        NSInteger row;
        if(indexPath.row <4){
            row = (indexPath.row - 1);
        }else{
            row = (indexPath.row - 2);
        }
        Gallery *gallery = [self.arrGallery objectAtIndex:row];
        if([gallery.strTitle isEqualToString:@"Recent Winners" ]){
             eventCompletionHandlerC(gallery,kWinner);
        }else{
             eventCompletionHandlerC(gallery,kDetails);
        }
       
    }
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"segueDetailCasino"]){
        DetailsVC *controller = (DetailsVC *)segue.destinationViewController;
        //controller.strTitle = @"Casino";
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        return 52;
    }else if(indexPath.row==4){
        return 52;
    }else{
        return 102;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
