//
//  Section.h
//  GoldCasino
//
//  Created by MAPPS MAC on 07/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Section : NSObject
@property(nonatomic)NSString *strSectionId;
@property(nonatomic)NSString *strSectionTitle;
- (id)initWithDictionary:(NSDictionary *)dict;
@end
