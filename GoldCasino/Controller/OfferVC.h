//
//  OfferVC.h
//  GoldCasino
//
//  Created by MAPPS MAC on 14/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypeO {
    kOfferTapMenu = 0,
    kOfferCasino = 1,
    kOfferDining = 2,
    kOfferEvent = 3,
    kOfferHotel = 4,
    kOfferNight = 5,
    kOfferTap=6,
    kOfferDetails=7,
    kOfferScan=8,
    kOfferNotify=9
};
typedef void (^EventCompletionHandlerO)(id object, NSUInteger EventTypeO);
@interface OfferVC : UIViewController{
     EventCompletionHandlerO eventCompletionHandlerO;
}
- (void)setEventOnCompletion:(EventCompletionHandlerO)handler ;
- (IBAction)btnMenuAction:(UIButton *)sender;
- (void)getOfferList;
@property (weak, nonatomic) IBOutlet UIView *viewTrans;

@end
