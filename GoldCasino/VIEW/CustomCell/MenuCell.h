//
//  MenuCell.h
//  GoldCasino
//
//  Created by MAPPS MAC on 19/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *leftImg;
@property (weak, nonatomic) IBOutlet UILabel *lblMenu;
@property (weak, nonatomic) IBOutlet UILabel *lblSep;

@end
