//
//  Offer.h
//  GoldCasino
//
//  Created by SKETCH_IOS_01 on 27/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Offer : NSObject
@property(nonatomic)NSString *strOfferId;
@property(nonatomic)NSString *strOfferName;
@property(nonatomic)NSString *strOfferDesc;
@property(nonatomic)NSString *strOfferCode;
@property(nonatomic)NSString *strOffervalidityperiod;
@property(nonatomic)NSString *strOfferExp;
@property(nonatomic)NSString *strOfferImage;
- (id)initWithDictionary:(NSDictionary *)dict;	
@end
//offer list
//{"Success":true,"msg":"Offer Found","OfferList":[{"OfferId":11,"OfferName":"Free Play Cash!","OfferDescription":"Free Play Cash","OfferCode":"FPC","OfferValidityPeriod":"","OfferExpDate":"1\/15\/2017 12:00:00 AM","OfferImage":"https:\/\/landingpage.experiture.com\/TrackingSystem\/AG_44625\/AD_44626\/Images\/all_off_1.jpg"}]}

