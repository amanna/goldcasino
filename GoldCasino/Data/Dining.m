//
//  Offer.m
//  GoldCasino
//
//  Created by SKETCH_IOS_01 on 27/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "Dining.h"

@implementation Dining

- (id)initWithDictionary:(NSDictionary *)dict{
    if (self = [super init]) {
              
    }
    return self;
}
- (id)constructDinner:(NSInteger)index{
    switch (index) {
        case 0:{
            self.strTitle = @"Banana Cabana";
            self.strSubTitle = @"Make tonight simply...";
            self.strDesc = @"<html><body><p><strong>Hours of Operation</strong></p><p>5PM - 1 am (Monday &amp; Tuesday)</p><p>5PM - 2 am (Wednesday &amp; Thursday)</p><p>2pm 10 pm (Saturday &amp; Sunday)</p><p>Closed (Friday)</p><strong>About us</strong><p>We are committed to satisfying our customers with great food and excellent service. We offer a wide variety of delicious dishes for Lunch and Dinner. Whatever you're in the mood for, you can find it here! Take a look at our wonderful menu and you'll find everything that you need! We hope to see you soon!</p></body></html>";
            self.strImage = @"BananaCabana.png";
            self.strLargeImage = @"BananaCabana-big.png";
        }
            break;
        case 1:{
            self.strTitle = @"Hot Sour and Soar";
            self.strSubTitle = @"World Class dining experience";
            self.strDesc = @"<html><body><p><strong>Hours of Operation</strong></p><p>11am &ndash; 10pm (daily)</p><strong>About us</strong><p>Enjoy the freshest Asian fusion cuisine with our top notch Chinese, Japanese, and Thai inspired dishes.  See what special rolls our sushi chef has prepared or try our dangerous Mai Tai cocktail!</p></body></html>";
            self.strImage = @"HotSourSoar.png";
             self.strLargeImage = @"HotSourSoar-big.png";
        }
            break;
        case 2:{
            self.strTitle = @"The Spinach Armada";
            self.strSubTitle = @"Vegetarians and Meat Eaters Rejoice… ";
            self.strDesc = @"<html><body><p><strong>Hours of Operation</strong></p><p>11am &ndash; 10pm (daily)</p><strong>About us</strong><p>We feature the finest ethically raised, organic meats and dairy alongside a bevy of vegetarian and vegan specialties.  Come enjoy classic American dishes crafted with a farm to table mentality.  Top choices include our Spicy Aioli Turkey Burger and Vegetarian Lasagna.  We also serve 12 types of award-winning, fair-trade coffee.</p></body></html>";
            self.strImage = @"TheSpinachArmada.png";
            self.strLargeImage = @"TheSpinachArmada-big.png";
        }
            break;
            
        default:
            break;
    }
    return self;

    
}
@end
