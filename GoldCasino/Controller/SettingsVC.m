//
//  SettingsVC.m
//  GoldCasino
//
//  Created by Amrita on 23/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "SettingsVC.h"
#import "POAcvityView.h"
@interface SettingsVC (){
     POAcvityView *activity;
}
@property (weak, nonatomic) IBOutlet UIWebView *myWeb;
- (IBAction)btnMenuAction:(UIButton *)sender;
- (IBAction)btnNoticeAction:(UIButton *)sender;

@end

@implementation SettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
    
    [self.myWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://app.xgoldcasino.com/setting_new_app.html"]]];
    
    [activity showView];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];

    // Do any additional setup after loading the view.
}
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [activity hideView];
    NSLog(@"finish");
}


-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    [activity showView];
    NSLog(@"Error for WEBVIEW: %@", [error description]);
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    eventCompletionHandlerSt(@"1",kTapSt);
}
- (void)setEventOnCompletionSt:(EventCompletionHandlerSt)handler{
    eventCompletionHandlerSt = handler;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnMenuAction:(UIButton *)sender {
   if(self.view.frame.origin.x==0){
        eventCompletionHandlerSt(@"0",kMenuSt);
    }else{
        eventCompletionHandlerSt(@"1",kTapSt);
    }
}

- (IBAction)btnNoticeAction:(UIButton *)sender {
     eventCompletionHandlerSt(nil,kStNotify);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
