//
//  WinnerVC.m
//  GoldCasino
//
//  Created by MAPPS MAC on 11/02/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "WinnerVC.h"
#import "POAcvityView.h"
@interface WinnerVC (){
    POAcvityView *activity;
}
@property (weak, nonatomic) IBOutlet UIWebView *myWebview;
- (IBAction)btnBackAction:(UIButton *)sender;


@end

@implementation WinnerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
    [self.myWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://app.xgoldcasino.com/recent_winner_iphone.html"]]];
    [activity showView];
    // Do any additional setup after loading the view.
}
- (void)setEventOnCompletionW:(EventCompletionHandlerW)handler{
    eventCompletionHandlerW = handler;
}
- (IBAction)btnBackAction:(UIButton *)sender {
    eventCompletionHandlerW(nil,kWBack);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [activity hideView];
    NSLog(@"finish");
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    [activity showView];
    NSLog(@"Error for WEBVIEW: %@", [error description]);
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
