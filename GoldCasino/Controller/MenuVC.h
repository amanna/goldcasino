//
//  MenuVC.h
//  GoldCasino
//
//  Created by MAPPS MAC on 15/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypeM {
    kActionSelect = 0
};
typedef void (^EventCompletionHandler)(id object, NSUInteger eventTypeM);
@interface MenuVC : UIViewController{
     EventCompletionHandler eventCompletionHandler;
}
- (void)setEventOnCompletion:(EventCompletionHandler)handler ;
@end
