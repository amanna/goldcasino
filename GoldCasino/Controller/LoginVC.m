//
//  LoginVC.m
//  GoldCasino
//
//  Created by MAPPS MAC on 25/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "LoginVC.h"
#import "WebServiceManager.h"
#import "CTextField.h"
#import "POAcvityView.h"
@interface LoginVC ()<UITextFieldDelegate>{
    POAcvityView *activity;
}
@property (weak, nonatomic) IBOutlet CTextField *txtUname;
@property (weak, nonatomic) IBOutlet CTextField *txtPass;
- (IBAction)btnCancelAction:(UIButton *)sender;

- (IBAction)btnLoginAction:(UIButton *)sender;
@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.txtUname.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtPass.autocorrectionType = UITextAutocorrectionTypeNo;
     activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
    self.txtUname.leftPadding = 63;
     self.txtPass.leftPadding = 63;
    // Do any additional setup after loading the view.
}
- (void)setEventOnCompletion:(EventCompletionHandlerL)handler{
    eventCompletionHandlerL = handler;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtUname) {
        [self.txtUname resignFirstResponder];
        [self.txtPass becomeFirstResponder];
    }else{
        [self.txtPass resignFirstResponder];
    }
    return YES;
}
- (IBAction)btnCancelAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnLoginAction:(UIButton *)sender {
//    [self dismissViewControllerAnimated:YES completion:^{
//        eventCompletionHandlerL(nil,kLoginAction);
//    }];
    [self.txtPass resignFirstResponder];
    [self.txtUname resignFirstResponder];
   if([self.txtUname.text isEqualToString:@""] || [self.txtPass.text isEqualToString:@""]){
       //[UtiltiyManager showAlertWithMessage:@"Please enter user name and password"];
       UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Login" message:@"Please enter user name and password" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
       [alertView show];
   }else{
       [activity showView];
      [WebServiceManager loginOnCompletion:self.txtUname.text andPass:self.txtPass.text onCompletion:^(id object, NSError *error) {
          [activity hideView];
          if(object){
              NSString *str = object;
              if([str isEqualToString:@"1"]){
                  [self dismissViewControllerAnimated:YES completion:^{
                              eventCompletionHandlerL(nil,kLoginAction);
                          }];
              }else{
                  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Login" message:@"Invalid User Name Or Password" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                  [alertView show];

              }
          }else{
              UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Login" message:@"Some Problem Occurs!!Please try again later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
              [alertView show];

          }
      }];
   }
}
@end
