//
//  Offer.h
//  GoldCasino
//
//  Created by SKETCH_IOS_01 on 27/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Dining: NSObject
@property(nonatomic)NSString *strTitle;
@property(nonatomic)NSString *strSubTitle;
@property(nonatomic)NSString *strDesc;
@property(nonatomic)NSString *strImage;
@property(nonatomic)NSString *strLargeImage;
- (id)initWithDictionary:(NSDictionary *)dict;
- (id)constructDinner:(NSInteger)index;
@end
//offer list
//{"Success":true,"msg":"Offer Found","OfferList":[{"OfferId":11,"OfferName":"Free Play Cash!","OfferDescription":"Free Play Cash","OfferCode":"FPC","OfferValidityPeriod":"","OfferExpDate":"1\/15\/2017 12:00:00 AM","OfferImage":"https:\/\/landingpage.experiture.com\/TrackingSystem\/AG_44625\/AD_44626\/Images\/all_off_1.jpg"}]}

